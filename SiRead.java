
import com.fazecast.jSerialComm.*;
import java.util.*;
import java.io.*;

public class SiRead{	
	
	enum Cip{
		SI5,
		SI6,
		SI8,
		SI10
	}
	
	static boolean pokracuj;
	static int vyctenychCipu = 0;
	static boolean vzdalenyRezim = false;
	
	public static void main(String[] args) {
		SerialPort port = nactiPort();
		while (!nastavRezimPripojeni(port)) {}
		pokracuj = true;
		while (pokracuj) {			
			vypisPrikazy();
			String prikaz = nactiPrikaz();
			vykonejPrikaz(prikaz, port);
		}
	}
	
	public static void vypisPrikazy() {
		System.out.println();
		System.out.println("Z: Zobraz aktualni cas");
		System.out.println("P: Prepis aktualni cas");
		if (!vzdalenyRezim) {
			System.out.println("V: Vycti data z cipu");
		}
		System.out.println("N: Nastav rezim pripojeni (primy/vzdaleny)");
		System.out.println("U: Ukonci program");
		System.out.println();
		System.out.println("Pro vykonani instrukce zadej prvni pismeno a stiskni enter:");
	}
	
	public static String nactiPrikaz() {
		Scanner sc = new Scanner(System.in);
		if(sc.hasNextLine()){
			return sc.nextLine().toUpperCase();
		}
		return null;
	}
	
	public static void vykonejPrikaz(String prikaz, SerialPort port) {
		byte[] prijem = null;
		byte[][] prijemZCipu = null;
		
		zpracovaniPrikazu:
		switch (prikaz) {
			case "P":
				prijem = posliPrikaz((byte) 0xF7, new byte[]{}, false, port);
				if(prijem!=null) {
					byte[] novyCas = prepisCas(prijem);
					if (novyCas != null) {
						byte[] prijem2 = null;
						do {						
							prijem2 = posliPrikaz((byte) 0xF6, novyCas, false, port);
							if(prijem2 == null) {
								prijem2 = new byte[12];
								continue;
							}
						} while (!Arrays.equals(Arrays.copyOfRange(prijem2, 5, 11), Arrays.copyOfRange(novyCas, 0, novyCas.length-1)));
						zobrazZjistenyCas(prijem2);
					}
				}				
				break;
			case "Z":
				prijem = posliPrikaz((byte) 0xF7, new byte[]{}, false, port);
				if(prijem!=null) {
					zobrazZjistenyCas(prijem);
				}
				break;
			case "V":
				if (vzdalenyRezim) {
					System.out.println("Prikaz lze pouzit pouze pri primem rezimu");
					break;
				}
				Cip cip = nactiCip(port);
				if(cip==null) {
					break;
				}
				switch (cip) {
					case SI5:
						prijemZCipu = new byte[1][128];
						prijem = posliPrikaz((byte) 0xB1, new byte[]{}, true, port);
						if (prijem != null) {
							prijemZCipu[0] = Arrays.copyOfRange(prijem,5,133);
						}
						break;
					case SI6:
						prijemZCipu = new byte[8][128];
						for (int i = 0; i < 8; i++) {
							prijem = posliPrikaz((byte) 0xE1, new byte[]{(byte)i}, true, port);
							if (prijem != null) {
								prijemZCipu[i] = Arrays.copyOfRange(prijem,6,134);
							}
						}
						break;
					case SI8:
						prijemZCipu = new byte[2][128];
						for (int i = 0; i < 2; i++) {
							prijem = posliPrikaz((byte) 0xEF, new byte[]{(byte)i}, true, port);
							if (prijem != null) {
								prijemZCipu[i] = Arrays.copyOfRange(prijem,6,134);
							}
						}
						break;
					case SI10:
						prijemZCipu = new byte[8][128];
						for (int i = 0; i < 8; i++) {
							prijem = posliPrikaz((byte) 0xEF, new byte[]{(byte)i}, true, port);
							if (prijem != null) {
								prijemZCipu[i] = Arrays.copyOfRange(prijem,6,134);
							}
						}
						break;
				}	
				
				posliPrikaz((byte) 0xF8, new byte[]{}, false, port);
				
				for (int i = 0; i < prijemZCipu.length; i++) {
					if (prijemZCipu[i]==null) {
						break zpracovaniPrikazu;
					}
				}
				zpracujDataCipu(prijemZCipu);
				break;
			case "N":
				nastavRezimPripojeni(port);
				break;
			case "U":
				pokracuj = false;
				break;
			default:
				System.out.println("Neplatny prikaz");
		}
	}
	
	public static void zpracujDataCipu(byte[][] prijem) {
		String nazevSouboru = String.format("cip%d.txt",vyctenychCipu);
		vyctenychCipu++;
		
		File soubor = new File(nazevSouboru);
		try {soubor.createNewFile();} catch (IOException e) {System.out.println(e.getMessage());};
		
		PrintWriter pw = null;
		try {pw = new PrintWriter(nazevSouboru);} catch (FileNotFoundException e) {System.out.println(e.getMessage());};
		for (int i = 0; i < prijem.length; i++) {
			for (int j = 0; j < prijem[i].length; j++) {
				pw.println(Byte.toUnsignedInt(prijem[i][j]));
			}
		}
		pw.close();
		System.out.printf("Data z cipu ulozena do souboru %s\n",nazevSouboru);
	}
	
	public static Cip nactiCip(SerialPort port) {
		System.out.println("Vloz cip do pripojene stanice.");
		
		if(!otevriPort(port)){
			System.out.println("Port nelze otevrit");
			return null;
		}
		
		int bytuNaPrijmu = 0;
		long casPocatek = System.currentTimeMillis();
		long casUbehl = 0;
		do {
			bytuNaPrijmu = port.bytesAvailable();
			casUbehl = System.currentTimeMillis() - casPocatek;
		} while (bytuNaPrijmu==0 && casUbehl<20000);
		if(bytuNaPrijmu==0) {
			System.out.println("Cip nebyl vlozen, navrat do menu.");
			return null;
		}
		
		byte[] prijem = new byte[bytuNaPrijmu];
		port.readBytes(prijem, bytuNaPrijmu);
		
		if(prijem.length<5) {
			System.out.println("Chyba pri prenosu dat");
			return null;
		}		
		
		if(!zkontrolujData(prijem)) {
			System.out.println("Chyba pri prenosu dat");
			return null;
		}
		
		switch (prijem[1]){
			case (byte) 0xE5:
				return Cip.SI5;
			case (byte) 0xE6:
				return Cip.SI6;
			case (byte) 0xE8:
				switch (prijem[5]) {
					case (byte) 0xEF:
						return Cip.SI10;
					default:
						return Cip.SI8;					
				}
			default:
				return null;
		}		
	}
	
	public static boolean zkontrolujData(byte[] prijem) {
		int[] kontrolniCrc = vypoctiCRC(prijem);
		if(kontrolniCrc[1]!=Byte.toUnsignedInt(prijem[prijem.length-3]) || kontrolniCrc[0]!=Byte.toUnsignedInt(prijem[prijem.length-2])) {
			return false;
		}
		return true;
	}
	
	public static SerialPort nactiPort() {
		SerialPort[] porty = SerialPort.getCommPorts();		
		SerialPort port = null;
		
		int pripojenych = 0;
		for (SerialPort p : porty) {
			if (p.getDescriptivePortName().contains("SPORTident")) {
				port = p;
				pripojenych++;
			}
		}
		
		switch (pripojenych) {
			case 0:
				System.out.println("Zadne zarizeni neni pripojeno. Pripojte zarizeni a spustte program znovu.");
				return null;
			case 1:
				System.out.printf("Aktualne pouzivany port: %s.\n",port.getDescriptivePortName());		
				return port;
			default:
				System.out.println("Je pripojeno vice zarizeni. Pripojte pouze jedno a spustte program znovu.");
				return null;
		}		
		
	}
	
	public static byte[] posliPrikaz(byte instrukce, byte[] data, boolean komunikaceSCipem, SerialPort port) {
		byte[] prikaz = sestavPrikaz(instrukce, data);
		
		OutputStream comVystup = port.getOutputStream();
		byte[] prijem = null;
		
		boolean opakuj;
		int i = 0;
		do{
			opakuj = false;
		
			if(!otevriPort(port)){
				return null;
			}
			
			try {comVystup.write(prikaz);} catch(IOException e) {System.out.println(e.getMessage());};
	
			try {comVystup.flush();} catch (IOException e) {System.out.println(e.getMessage());};
			
			int bytuNaPrijmu = 0;
			long casPocatek = System.currentTimeMillis();
			long casUbehl = 0;
			int timeout;
			
			if (komunikaceSCipem) {
				timeout = 2000;
				do {
					bytuNaPrijmu = port.bytesAvailable();
					casUbehl = System.currentTimeMillis() - casPocatek;
				} while (bytuNaPrijmu==0 && casUbehl<timeout);
				if(bytuNaPrijmu!=12) {
					try{
						Thread.sleep(10);
					}
					catch(InterruptedException ex){
						Thread.currentThread().interrupt();
					}
					bytuNaPrijmu = port.bytesAvailable();
				}
			}else {				
				timeout = 200;
				do {
					bytuNaPrijmu = port.bytesAvailable();
					casUbehl = System.currentTimeMillis() - casPocatek;
				} while (bytuNaPrijmu==0 && casUbehl<timeout);
			}
			
			if(bytuNaPrijmu<7){
				opakuj = true;
			}else{
				prijem = new byte[bytuNaPrijmu];
				System.out.printf("Probiha cteni %d bytu...\n", bytuNaPrijmu);
				port.readBytes(prijem, bytuNaPrijmu);
				
				if (prijem[prijem.length-1]!=(byte)0x03) {
					opakuj = true;
				}
			}
			
			port.closePort();	
			i++;
			
		} while (opakuj && i<=12);
		
		if(i>12) {
			System.out.printf("Nedari se navazat komunikaci se stanici.");
			if(vzdalenyRezim) {
				System.out.printf(" Je druha (vzdalena) stanice skutecne pritomna?");
			}
			System.out.printf("\n");
			return null;
		}	
		if(!zkontrolujData(prijem)) {
			System.out.println("Chyba pri prenosu dat");
			return null;
		}
		
		
		return prijem;
	}
	
	public static void zobrazZjistenyCas(byte[] prijem) {
		if (prijem==null) {
			return;
		}
		int[] data = new int[prijem.length];
		for (int i = 0; i<prijem.length; i++) {
			data[i] = Byte.toUnsignedInt(prijem[i]);
		}
		
		int cas = data[9]*256 + data[10];
		int hodina12 = (int) Math.floor(cas/3600);
		int hodina24 = hodina12 + 12*(data[8] % 2);
		int minuta = (int) Math.floor((cas-hodina12*3600)/60);
		int sekunda = cas-hodina12*3600-minuta*60;
		
		System.out.println("Aktualne nastaveny cas:");
		System.out.printf("%d.%d.20%d, %02d:%02d:%02d\n",data[7],data[6],data[5],hodina24,minuta,sekunda);
	}
	
	public static byte[] prepisCas(byte[] prijem) {
		int[] data = new int[prijem.length];
		for (int i = 0; i<prijem.length; i++) {
			data[i] = Byte.toUnsignedInt(prijem[i]);
		}
		
		int cas = data[9]*256 + data[10];
		int hodina12 = (int) Math.floor(cas/3600);
		int hodina24 = hodina12 + 12*(data[8] % 2);
		int minuta = (int) Math.floor((cas-hodina12*3600)/60);
		int sekunda = cas-hodina12*3600-minuta*60;
		
		System.out.println("Aktualni cas ve formatu dd mm yy hh mm ss:");
		System.out.printf("%02d %02d %02d %02d %02d %02d\n",data[7],data[6],data[5],hodina24,minuta,sekunda);
		System.out.println("Zadej nove nastaveni casu (ve stejnem formatu dd mm yy hh mm ss):");
		Scanner sc = new Scanner(System.in);
		String vstup = "";
		if (sc.hasNextLine()) {
			vstup = sc.nextLine();
		}
		String[] rozdelenyVstup = vstup.split(" ");
		if (rozdelenyVstup.length != 6) {
			System.out.println("Neplatny format vstupu");
			return null;
		}
		
		int[] ciselnyVstup = new int[rozdelenyVstup.length];
		for (int i = 0; i < rozdelenyVstup.length; i++) {
			try {ciselnyVstup[i] = Integer.parseInt(rozdelenyVstup[i]);} catch (NumberFormatException e) {System.out.println("Zadane udaje nejsou cela cisla"); return null;};
			if (ciselnyVstup[i]<0){
				System.out.println("Jako cas nelze zadat zaporna cisla.");
				return null;
			}
		}
		if (ciselnyVstup[0]<1 || ciselnyVstup[0]>31) {
			System.out.println("Spatny format zadanych dni.");
			return null;
		}else if (ciselnyVstup[1]<1 || ciselnyVstup[1]>12) {
			System.out.println("Spatny format zadanych mesicu.");
			return null;
		}else if (ciselnyVstup[3]>23) {
			System.out.println("Spatny format zadanych hodin.");
			return null;
		}else if (ciselnyVstup[4]>59) {
			System.out.println("Spatny format zadanych minut.");
			return null;
		}else if (ciselnyVstup[5]>59) {
			System.out.println("Spatny format zadanych sekund.");
			return null;
		}
		
		byte[] novyCas = new byte[7];
		novyCas[0] = (byte) ciselnyVstup[2];
		novyCas[1] = (byte) ciselnyVstup[1];
		novyCas[2] = (byte) ciselnyVstup[0];				
		int novaHodina = ciselnyVstup[3];
		if (novaHodina > 11) {
			novyCas[3] = (byte) (data[8] - (data[8] % 2) + 1);
			novaHodina -= 12;
		}else {
			novyCas[3] = (byte) (data[8] - (data[8] % 2));
		}
		int novychSekund = 3600*novaHodina + 60*ciselnyVstup[4] + ciselnyVstup[5];
		novyCas[4] = (byte) ( (int)Math.floor(novychSekund/256) );
		novyCas[5] = (byte) (novychSekund - (int)Math.floor(novychSekund/256)*256);
		novyCas[6] = (byte) data[11];
		
		return novyCas;			
	}	
	
	public static boolean otevriPort(SerialPort port) {
		if (!port.isOpen()) {
			if (!port.openPort()) {
				System.out.println("Port nejde otevrit");
				return false;
			}
		}
		
		if (port.getBaudRate()!=38400){
			port.setBaudRate(38400);
		}
		return true;
	}
	
	public static byte[] sestavPrikaz(byte instrukce, byte[] data) {
		byte[] prikaz = new byte[8+data.length];
		prikaz[0] = (byte) 0xFF;
		prikaz[1] = (byte) 0x02;
		prikaz[2] = (byte) 0x02;
		prikaz[3] = instrukce;
		prikaz[4] = (byte) data.length;
		for (int i = 0; i<data.length;i++) {
			prikaz[i+5] = data[i];
		}

		int[] crc = vypoctiCRC(prikaz);
		prikaz[prikaz.length-3] = (byte) crc[1];
		prikaz[prikaz.length-2] = (byte) crc[0];
		prikaz[prikaz.length-1] = (byte) 0x03;
		
		return prikaz;
	}
	
	public static int[] vypoctiCRC(byte[] prikaz) {
		int[] vysledek = new int[2];
		
		int nadbytecnychPrikazu = 0;
		for (int i = 0; i<prikaz.length; i++) {
			if (prikaz[i]!=(byte)0xFF && prikaz[i]!=(byte)0x02) {
				nadbytecnychPrikazu = 3+i;
				break;
			}
		}
		
		if((prikaz.length-nadbytecnychPrikazu) < 1) {
			return null;
		}
		
		byte[] vypocetCRC = new byte[prikaz.length-nadbytecnychPrikazu];
		for (int i = 0; i<vypocetCRC.length; i++) {
			vypocetCRC[i] = prikaz[i+(nadbytecnychPrikazu-3)];
		}
		int crc = CRCCalculator.crc(vypocetCRC);
		String crc1String = String.format("%04x", crc).substring(0,2);
		String crc0String = String.format("%04x", crc).substring(2,4);
		vysledek[1] = Integer.parseInt(crc1String, 16);
		vysledek[0] = Integer.parseInt(crc0String, 16);
		return vysledek;
	}
	
	public static boolean nastavRezimPripojeni(SerialPort port) {
		System.out.println("P: Primy");
		System.out.println("V: Vzdaleny");
		System.out.println();
		System.out.println("Pro vyber rezimu pripojeni zadej prvni pismeno a stiskni enter:");
		
		String vstup = nactiPrikaz();
		byte[] prijem = null;
		int cisloStanice;
		int i;
		switch (vstup) {
			case "P":
				i = 0;
				do {
					prijem = posliPrikaz((byte)0xF0, new byte[]{(byte)0x4D}, false, port);
					i++;
				} while (prijem == null && i < 5);
				
				if (prijem != null) {				
					cisloStanice = 256*Byte.toUnsignedInt(prijem[3]) + Byte.toUnsignedInt(prijem[4]);
					System.out.printf("Probiha prima komunikace se stanici cislo %d", cisloStanice);
					vzdalenyRezim = false;
					return true;
				} else {
					return false;
				}
				
			case "V":
				i = 0;
				do{
					prijem = posliPrikaz((byte)0xF0, new byte[]{(byte)0x53}, false, port);
					i++;
					try{
						Thread.sleep(20);
					}
					catch(InterruptedException ex){
						Thread.currentThread().interrupt();
					}
				} while (prijem == null && i < 5);
				
				if (prijem != null) {					
					cisloStanice = 256*Byte.toUnsignedInt(prijem[3]) + Byte.toUnsignedInt(prijem[4]);
					System.out.printf("Probiha vzdalena komunikace pres stanici cislo %d", cisloStanice);
					vzdalenyRezim = true;
					return true;
				}else{
					return false;
				}
				
			default:
				System.out.println("Neznamy prikaz");
				return false;
		}
	}
}